console.log("Script injected.");
function isOffline(){
  return document.querySelector(".radioplayer-erroroverlay h1") !== null;
}

function handleTimeout(){
  setTimeout(function(){
    if(isOffline()){
      console.log("iPlayer has gone offline - reloading page...")
      window.location.reload();
    } else{
      console.log("iPlayer is still online")
      handleTimeout();
    }
  },2000)
}

handleTimeout();
